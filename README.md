# netbsd-live

## Customized live releases

1.) default: classic netbsd live

2.) EFI-Ramdisk, with Boot loader and grub2 preinstalled. 

3.) V5 works very fine with legacy but it can be as well used for EFI.
    If you need EFI with efi lockdown, just visit: https://gitlab.com/openbsd98324/netbsd-live-efi


5.) the default one, finalized, tested and very stable - Version 5:
````
https://gitlab.com/openbsd98324/netbsd-live/-/raw/master/live/9.1/amd64/v5/netbsd-live-image-2000mb-v5.img.gz
````


## Media 

The NetBSD Live desktop (started with: startx)

![](netbsd-live.png)



## Current (Stable)

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/netbsd-live/-/raw/master/live/9.1/amd64/v5/netbsd-live-image-2000mb-v5.img.gz"

## Release Versions 

1.) *Version 1:* is the minimal, out the box release. It has fstab with sd1a running NetBSD.  


2.) *Version 2:* is the second release with NetBSD. It has fstab with ROOT.a. This is the *Standard* stable version. This version will work on any AMD64 machine. 


3.) *Version 3:* is the version with grub2 and a live ramdisk rescue system. It has fstab with ROOT.a. This is the *Standard* stable version with Grub2. This version will work on any AMD64 machine.  


4.) *Version 4:* is a smaller, less than 1.9 TB, with default netbsd bootloader (working on most amd64 machines). This image will fit on memstick or SD/MMC less than 2 TB.


5.) *Version 5:* is a portable netbsd-live, with grub2, and with a Linux kernel for rescue.
    About 700 Mb, with several installers, including netbsd-INSTALL.gz
    The env X11 is working fine on Intel and AMD machines. Just type: startx. The NetBSD /boot.cfg is designed to have a running X11/X environment powered by NetBSD, which is readily stable, working on Intel and AMD64.


## Installation of Linux

This tool needs rawrite32 to create the memstick or SD/MMC card (sandisk, mmc,... mmcblk0p2...) with the live.
Once the Live is on the memstick or SD/MMC, one can start Slackware from Ramdisk.
The fdisk /dev/sdX or /dev/mmcblk0  will allow to create a new partition. Followed by mkfs.ext3.
Once the partition created or formatted. It is easy to establish the network with dhcpcd and 
to download the base system. The devuan (DEVUAN.tar.gz) file can be downloaded with a barebone 
 content, made with debootstrap and with a vmlinuz/modules, ready out of the box.

1.) Rawrite32 for Windows: 
https://gitlab.com/openbsd98324/rawrite/-/blob/master/rawrite-1.0.9.0-win.zip

If you find something that runs Linux or BSD, this is easier, e.g.:   zcat file.img.gz > /dev/sdX   for Linux    or  zcat file.img.gz > /dev/rsd0    for  NetBSD. 


2.) Download the grub2 and live, and create with rawrite32 the memstick or sd/mmc device to boot grub2:
https://gitlab.com/openbsd98324/netbsd-live/-/blob/master/live/9.1/amd64/v5/netbsd-live-image-2000mb-v5.img.gz

3.) 
The classic: tar xvpfz ... and mv DEVUAN/* to the location (root) of your partition.
https://gitlab.com/openbsd98324/devuan-live/-/blob/master/v1/ascii/amd64/DEVUAN.tar.gz

Once done, give the information (correct disk e.g (hd0,msdos2) and the device e.g. /dev/mmcblk0p2) to Grub2.
Boot and login/passwd with simply "root".

The rest is then apt-get update ; apt-get install gcc clang ... and so on.


## Installation of netbsd

The image is delivered out of the box with netbsd live.



## Note for NetBSD Live

Login: netbsd 

Password:  ` reverse the md5 ... to find the pass.... 0325f1f79b7e17d57132300ef4d31fe3 `



    Have Fun!






