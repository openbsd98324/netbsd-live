## Download 

wget the file 
netbsd-live-image-2000mb-v7-slack-grub-netbsd-9.1-devuan-v7.qcow2.tar.gz 


Command #1:

wget -c --no-check-certificate "https://gitlab.com/openbsd98324/netbsd-live/-/raw/master/live/9.1/amd64/v7-qcow2/netbsd-live-image-2000mb-v7-slack-grub-netbsd-9.1-devuan-v7.qcow2.tar.gz"

Command #2: 

tar xvpfz netbsd-live-image-2000mb-v7-slack-grub-netbsd-9.1-devuan-v7.qcow2.tar.gz


## Install on VMM 


copy the file qcow2


cp netbsd-live-image-2000mb-v7-slack-grub-netbsd-9.1-devuan-v7.qcow2  myusername.qcow2

## Serial and VMM Hypervisor 

You are using VMM? 

Then, you need to use console=... for linux  or  consdev=com0 for NetBSD. 



## GRUB

vmctl start -c myusername 

The grub will need : 

console=tty0  console=ttyS0 

in order to run well under VMM hypervisor.

(see : https://gitlab.com/openbsd98324/netbsd-live/-/blob/master/live/9.1/amd64/v7-qcow2/grub.cfg.hypervisor2 )

